(function ($) {
  /* Ajax reloading */
  Drupal.behaviors.AjaxReloading = {
    attach: function(context, settings) {
      $("body").once(function() {
        setInterval(function() {
          var ajax = new Drupal.ajax(false, false, {
            url : '/ajax/reload/node',
            submit: {
              'nid': settings.AjaxReload.node_nid,
              'condition': settings.AjaxReload.condition
            }
          });
          ajax.eventResponse(ajax, {});
        }, settings.AjaxReload.timeInterval*1000);
      });
    }
  };
})(jQuery);
